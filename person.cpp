#include "person.h"
#include <QDate>

Person::Person(QString firstName, QString lastName,QString email,
               QString registration,QString password,QObject *parent) : QObject(parent)
{
    m_firstName = firstName;
    m_lastName = lastName;
    m_email = email;
    m_registration = registration;
    m_password = password;
    m_penalty = 0;
    m_books = new QQmlObjectListModel<Book>(this);
    m_returnbooks = new QQmlObjectListModel<Book>(this);
}

void Person::NewBook(QString name, QString author, QString edition, QDate dateRent, int quantity){

    Book *b = new Book(name, author, edition, dateRent, quantity, this);

    b->set_status(1);
    // aqui é preciso declarar a data de devolução do livro?? Como fazer??
    // Fazer a data de devolução aparecer na homepage só
    m_books->append(b);//Assim pode né?

}

// AO RENOVAR DAMOS UM PRAZO DE MAIS 7 DIAS PARTINDO DA DATA ATUAL
void Person::RenewBook(QString name, QString author, QString edition, QDate dateRent, int quantity){
    for(int i = 0; i<m_books->size(); i++){
        Book *b = m_books->at(i);

        if(b->get_author() == author && b->get_name() == name && b->get_edition() == edition){

            QDate aux = QDate::currentDate();
            aux.addDays(7);

            b->set_qdateRent(aux);

            /*if(b->get_date() + 7 > 30){
                b->set_date((7 - (30 - b->get_date())));
            }
            else{
                b->set_date(b->get_date() + 7);
            }*/
            break;
        }

    }
    qDebug("livro não encontrado");
}

void Person::updatePenalty()
{
    for(int i = 0; i<m_books->size(); i++){

        Book *b = m_books->at(i);
        QDate aux = QDate::currentDate();

        if(aux > b->get_qdateRent()) {
            m_penalty += b->get_qdateRent().daysTo(aux);
        }
    }
}


void Person::Historic(){

    for(int i =0; i<m_returnbooks->size(); i++){
        Book *b = m_returnbooks->at(i);
        qDebug() << "nome: " <<  b->get_name() << "autor: " << b->get_author();
    }

}
// PRECISA SABER COMO  VAI DESLIGAR O LIVRO NO BANCO DE DADOS E DEPOIS LIGA A LISTA DE LIVROS RETORNADOS
void Person::returnedBook(QString name, QString author, QString edition){
    Book *a;
    for(int i=0; i<m_books->size(); i++){
        a = m_books->at(i);
        if(a->get_name() == name){
            m_books->remove(m_books->at(i));
            break;
        }
    }
    QDate aux = QDate::currentDate();
    qDebug("livro devolvido" + aux.toString().toLatin1());
    m_returnbooks->append(new Book(name, author, edition, aux, 1, this));
    //Book *b = livro devolvido
    //b->set_status(0);

}

