import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.1

Item {
    id: windowDonation
    height: root.height
    width: root.width

    Rectangle{
        width: 300
        height: 300
        anchors.centerIn: windowDonation
        Popup{
            id: donationPopup

            property var book: Object

            function validate(nameBook, authorBook, editionBook, quantityBook) {
                book["nameBook"] = nameBook
                book["authorBook"] = authorBook
                book["editionBook"] = editionBook
                book["quantityBook"] = quantityBook
                donationPopup.open()
            }

            width: parent.width
            height: parent.height
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Column{
                anchors.centerIn: parent
                spacing: 5
                TextField{
                    id: adminKey
                    placeholderText: "Chave do DIACOM"
                    echoMode: TextInput.Password
                    width: donationPopup.width*0.8

                }
                TextField{
                    id: adminName
                    placeholderText: "Responsável do DIACOM"
                    width: donationPopup.width*0.8

                }
                Button{
                    id: adminButton
                    text: "Validar"
                    width: donationPopup.width*0.8
                    onClicked: {
                        if(personcontroller.confirmKey(adminKey.text)){
                         var aux = datacontroller.newBookRegister(donationPopup.book["nameBook"],donationPopup.book["authorBook"],donationPopup.book["editionBook"], donationPopup.book["quantityBook"])
                            donationPopup.close()
                            texterror.visible = false;
                            adminKey.text = "";
                            adminName.text = "";

                        }
                        else{
                            texterror.visible = true;
                        }
                    }
                }
                Text{
                    id: texterror
                    text: qsTr("Chave Errada!")
                    color: Material.accent
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
            }
        }
    }





    Connections{
        target:datacontroller
        onBookSuccess: {                              //SLOTS SÓ PODEM COMEÇAR COM LETRA MAISCULAS
            console.log("Livro cadastrado com sucesso!")
            textBookConfirm.visible = true
            textBookError.visible = false
            nameBookRegister.text = ""
            autorNameBookRegister.text = ""
            editionNameBookRegister.text = ""
            quantityBookRegister.value = 0
        }

        onBookNotSuccess: {
            console.log("Erro ao cadastrar!")
            textBookConfirm.visible = false
        }
    }

    Rectangle {
        height: root.height
        width: root.width
        color: "#B2DFDB"

        Pane {
            id: doanteRectangle
            height: parent.height*0.5
            width: parent.height*0.6
            anchors.centerIn: parent
            Material.elevation: 15

            Column{
                id: columnRegister
                height: parent.height
                width: parent.height*0.9
                anchors.centerIn: parent
                spacing: 10

                Text {
                    id: textdoacao
                    text: qsTr("Doe um livro!")
                    color: "gray"
                    font.bold: true
                    font.pointSize: 18
                }
                TextField{
                    id: nameBookRegister
                    placeholderText: qsTr("Título")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                    onTextChanged: {
                        if(nameBookRegister.text != ""){
                        textBookConfirm.visible = false;
                        }
                    }
                }
                TextField{
                    id: autorNameBookRegister
                    placeholderText: qsTr("Autor")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                }
                TextField{
                    id: editionNameBookRegister
                    placeholderText: qsTr("Edição")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                }
                SpinBox{
                    id: quantityBookRegister
                    focusReason: Qt.MouseFocusReason
                    width: parent.width*0.4
                }
                Text{
                    id: textBookConfirm
                    text: qsTr("Obrigado por doar!")
                    color: "green"
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
                Text {
                    id: textBookError
                    text: ""
                    color: "red"
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
                Ibutton{
                    id: registerBookButton
                    sizewidth: parent.width
                    sizeheight: nameBookRegister.height
                    text: "DOAR"
                    colorUp: Material.primary
                    colorDown:"#009688"
                    action.onClicked: {

                        if(nameBookRegister.text == "") {
                            textBookError.text = "Título obrigatório"
                            textBookError.visible = true;
                            textBookConfirm.visible = false;
                        } else if(autorNameBookRegister.text == "") {
                            textBookError.text = "Autor obrigatório!"
                            textBookError.visible = true;
                            textBookConfirm.visible = false;
                        } else if(editionNameBookRegister.text == "") {
                            textBookError.text = "Edição obrigatória!"
                            textBookError.visible = true;
                            textBookConfirm.visible = false;
                        } else if(quantityBookRegister.value == 0) {
                            textBookError.text = "Defina a quantidade de exemplares!"
                            textBookError.visible = true;
                            textBookConfirm.visible = false;
                        } else {
                            var i = quantityBookRegister.value;
                            console.log(i)
                            donationPopup.validate(nameBookRegister.text, autorNameBookRegister.text,
                                                          editionNameBookRegister.text, quantityBookRegister.value)
                        }
                    }
                }
            }
        }
    }
}
