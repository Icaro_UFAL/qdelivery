#include "book.h"

Book::Book(QString name, QString author, QString edition, QDate qdateRent, int quantity, QObject *parent) : QObject(parent)
{
    m_name = name;
    m_author = author;
    m_edition = edition;
    m_qdateRent = qdateRent;
    m_dateRent = qdateRent.toString("dd.MM.yyyy");
    m_quantity = quantity;
    m_maxRenew = 0;
    m_booksunavailable = 0;
    m_penalty = 1;
}
// STATUS =1 SIGNIFICA ESTAR ALUGADO,
//total / disponiveis
//disponiveis = total - alugados
