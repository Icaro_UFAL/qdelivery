#include "database.h"

Database::Database(QObject *parent) : QObject(parent)
{
    m_users = new QQmlObjectListModel<Person>(this);
    m_allbooks = new QQmlObjectListModel<Book>(this);
    Person *constantePerson = new Person("Icaro", "Gabriel", "admin", "1", "1");
    QDate aux = QDate::currentDate();
    qDebug("Data atual: " + aux.toString().toLatin1());
    //constantePerson->NewBook("Calculo V1","James Stwart", "Edicao 6", aux.addDays(7), 1);
    //constantePerson->NewBook("Geometria Analítica","Reis e Silva", "Edicao 5", aux.addDays(7), 1);
    m_users->append(constantePerson);


    // ADICIONAR ACERVO FICTICIO PARA TESTES
    Book *book1 = new Book("Cálculo Vol. 1", "Rafael Stewart", "6ª edição", aux, 5);
    Book *book2 = new Book("Cálculo Vol. 2", "Krerley", "6ª edição", aux, 2);
    Book *book3 = new Book("Geometria Analítica", "Reis e Silva", "5ª edição", aux, 3);
    Book *book4 = new Book("Álgebra Abstrata", "Jaime Evaristo", "3ª edição", aux, 2);
    Book *book5 = new Book("Álgebra Linear", "Elon Lages", "1ª edição", aux, 1);
    Book *book6 = new Book("Introducao a Lógica", "Fabio Paraguaçú", "4ª edição", aux, 2);
    Book *book7 = new Book("OpenIntro Statistics", "Heitor Ramos", "Capa dura", aux, 3);
    addBook(book1);
    addBook(book2);
    addBook(book3);
    addBook(book4);
    addBook(book5);
    addBook(book6);
    addBook(book7);
}

Database* Database::instance = NULL;

Database* Database::getInstance(){
    if(instance == NULL){
        instance = new Database();
    }
    return instance;
}

bool Database::addUser(Person *p)
{
    Person *a;

    //verifica se matricula ja foi utilizada em outro cadastro
    for(int i=0; i<m_users->size(); i++){

        a = m_users->at(i);

        if(p->get_registration() == a->get_registration() || p->get_email() == a->get_email()){
            qDebug() << "Usuário já cadastrado";
            return false;
        }
    }

    m_users->append(p);
    qDebug() << "Usuário cadastrado com sucesso";
    return true;
}

bool Database::addBook(Book *b){
    Book *a;
    for(int i=0; i<m_allbooks->size(); i++){
        a = m_allbooks->at(i);
        if(a->get_name() == b->get_name() && a->get_author() == b->get_author() && a->get_edition() == b->get_edition()){
            a->set_quantity(a->get_quantity() + 1);
            // SE JA EXISTE O LIVRO E ELE AUMENTOU SÓ A QUANTIDADE, A FUNÇÃO AUMENTA A QUANTIDADE E PARA
            return true;
        }
    }
    m_allbooks->append(b);
    return true;
}

bool Database::linkedBook(Person *p, Book *b, int type)
{
    Person *a;
    Book *c;
    bool flag = 0;
    // acha o usuário
    for(int i=0; i<m_users->size(); i++){
        a = m_users->at(i);
        if(p->get_registration() == a->get_registration()){
    // Faço um ponteiro apontar para o livro contido no acervo
            for(int j=0; j<m_allbooks->size(); j++){
                c = m_allbooks->at(j);
                if(c->get_name() == b->get_name() && c->get_author() == b->get_author() && c->get_edition() == b->get_edition()){
                  flag = 1;
                  break;
                }
            }
            break;
        }
    }
    // Se for para alugar, crio um novo livro no para o usuário atual e diminuo um no acervo
    if(type == 1 && flag == 1){
        if(c->get_quantity()-c->get_booksunavailable()){
            a->NewBook(b->get_name(), b->get_author(), b->get_edition(), b->get_qdateRent(), 1);
            //c->set_quantity(c->get_quantity() - 1);
            c->set_booksunavailable(c->get_booksunavailable() + 1);
            return true;
        }
    }
    // processo inverso...
    else if(type == 2 && flag == 1){
           a->returnedBook(c->get_name(), c->get_author(), c->get_edition());
           //c->set_booksavailable(c->get_booksavailable() + 1);
           c->set_booksunavailable(c->get_booksunavailable() - 1);
           return true;
    }
    return false;
}

//c - livro do acervo
//a - pessoa do banco
//b - livro da lista
