import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.1

Item {
    id: windowCollection
    height: root.height
    width: root.width
    signal sucesso;
    Rectangle{
        width: 300
        height: 300
        anchors.centerIn: windowCollection
        Popup{
            id: popup

            property var book: Object

            function validate(nameBook, authorBook, editionBook) {
                book["nameBook"] = nameBook
                book["authorBook"] = authorBook
                book["editionBook"] = editionBook
                popup.open()
            }

            width: parent.width
            height: parent.height
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Column{
                anchors.centerIn: parent
                spacing: 5
                TextField{
                    id: adminKey
                    placeholderText: "Chave do DIACOM"
                    echoMode: TextInput.Password
                    width: popup.width*0.8

                }
                TextField{
                    id: adminName
                    placeholderText: "Responsável do DIACOM"
                    width: popup.width*0.8

                }
                Button{
                    id: adminButton
                    text: "Validar"
                    width: popup.width*0.8
                    onClicked: {
                        if(personcontroller.confirmKey(adminKey.text)){
                            personcontroller.rentBook(popup.book["nameBook"],popup.book["authorBook"],popup.book["editionBook"])
                            popup.close()
                            texterror.visible = false;
                            adminKey.text = "";
                            adminName.text = "";

                        }
                        else{
                            texterror.visible = true;
                        }
                    }
                }
                Text{
                    id: texterror
                    text: qsTr("Chave Errada!")
                    color: Material.accent
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
            }
        }
    }

    Rectangle {
        height: root.height
        width: root.width
        color: "#B2DFDB"

        Rectangle {

            id: searchRectangle
            width: root.width*0.9; height: root.height*0.9
            anchors.centerIn: parent
            color: "transparent"

            Label {
                id: labelConsulta
                anchors.top: parent.top
                width: parent.width; height: parent.height/10
                text: "Consulta ao acervo:";
                font.bold: true;
                font.pixelSize: 36;
                horizontalAlignment: Text.AlignLeft; verticalAlignment: Text.AlignVCenter
            }

            TextField {
                id: textBuscar
                placeholderText: "Busca"
                width: parent.width*0.93
                anchors.top: labelConsulta.bottom

                onTextChanged: {
                    var bookList = database.allbooks
                    var searchedTextField = textBuscar.text
                    showedList.clear()

                    if(filtroTitulo.checked){                          //buscar pelo nome
                        if(textBuscar.text === ''){
                            bookListView.model = database.allbooks
                            return;
                        }
                        for(var i =0; i<bookList.size(); i++){
                            var actualBook = bookList.get(i)
                            if(actualBook.name.toLowerCase().indexOf(searchedTextField.toLowerCase()) !== -1){
                                showedList.append(actualBook)
                            }
                        }
                        bookListView.model = showedList
                    }
                    else{                                             //buscar pelo autor
                        if(textBuscar.text === ''){
                            bookListView.model = database.allbooks
                            return;
                        }
                        for(var i =0; i<bookList.size(); i++){
                            var actualBook = bookList.get(i)
                            if(actualBook.author.toLowerCase().indexOf(searchedTextField.toLowerCase()) !== -1){
                                showedList.append(actualBook)
                            }
                        }
                        bookListView.model = showedList
                    }
                    cleanerTimer.restart()
                }
            }
            Timer {

                id:cleanerTimer
                interval: 30*1000/*20 sec*/; running: true; repeat: false
                onTriggered: textBuscar.text = ""

            }
            Button{
                id: limparText
                text: "X"
                width: textBuscar.height
                height: textBuscar.height
                anchors.right: parent.right
                anchors.top: labelConsulta.bottom

                //colocar imagem

                onClicked: {
                    textBuscar.text = ""
                }
            }

            Label {
                id: labelFiltro
                width: textBuscar.width*0.08
                height: textBuscar.height
                anchors.top: textBuscar.bottom
                verticalAlignment: Text.AlignVCenter
                text: "Filtro:"
                font.pointSize: 12
            }

            RowLayout {
                id: filtroButtons
                anchors.top: textBuscar.bottom
                anchors.left: labelFiltro.right

                RadioButton {
                    id: filtroTitulo
                    text: "Título"
                    checked: true

                }

                RadioButton {
                    id: filtroAutor
                    text: "Autor"

                }
            }

            Item{
                id: itemListView
                width: parent.width
                anchors.top: filtroButtons.bottom
                anchors.topMargin: 20
                anchors.bottom: searchRectangle.bottom
                clip: true
                ListView{
                    id: bookListView
                    anchors.fill: parent
                    ListModel{id: showedList}
                    model: database.allbooks
                    spacing: parent.height*0.02
                    cacheBuffer: 3
                    delegate: Rectangle{
                        color: "transparent"
                        width: itemListView.width; height: itemListView.height*0.25
                        Image {
                            id: bookPhoto
                            width: parent.height*0.8
                            height: parent.height
                            source: "qrc:/im/iconNoPhoto.png"
                        }
                        Label{
                            id: nameBook
                            anchors.left: bookPhoto.right; anchors.leftMargin: 10
                            anchors.top: parent.top
                            font { pixelSize: 20 }
                            text: name; width: parent.width*0.42; height: parent.height*0.4
                        }
                        Label{
                            id: authorBook
                            anchors.left: bookPhoto.right; anchors.leftMargin: 10
                            width: parent.width*0.5; height: parent.height*0.2
                            font { pixelSize: 18 }
                            text: author; anchors.verticalCenter: bookPhoto.verticalCenter
                        }
                        Label {
                            id: editionBook
                            width: parent.width*0.5; height: parent.height*0.2
                            text: edition; font { pixelSize: 18 }
                            anchors.left: bookPhoto.right
                            anchors.leftMargin: 10
                            anchors.bottom: parent.bottom
                            anchors.bottomMargin: 3
                        }
                        Item{
                            id: itemAction
                            width: parent.width*0.3
                            height: parent.height
                            anchors.top: parent.top
                            anchors.bottom: parent.bottom
                            anchors.right: parent.right
                            Ibutton{
                                id: buttonnew
                                sizewidth: parent.width
                                sizeheight: parent.height*0.35
                                anchors.margins: 10
                                anchors.right: parent.right
                                anchors.top: parent.top
                                font.bold: true
                                text: "Alugar"
                                colorUp: Material.primary
                                colorDown: Material.accent

                                action.onClicked: {
                                    popup.validate(nameBook.text, authorBook.text, editionBook.text)
                                }
                            }

                            Image {
                                id: bookOk
                                source: "qrc:/im/check.png"
                                anchors.top: buttonnew.bottom
                                anchors.left: parent.left
                                anchors.leftMargin: buttonnew.width*0.2


                            }
                            Label{
                                id: numberOk
                                // width: parent.width*0.2; height: parent.height*0.2
                                text: quantity-booksunavailable; font { pixelSize: 15; bold: true }
                                anchors.top: bookOk.bottom
                                anchors.horizontalCenter: bookOk.horizontalCenter
                            }
                            Image {
                                id: bookNotOk
                                source: "qrc:/im/cross.png"
                                anchors.top: buttonnew.bottom
                                anchors.left: bookOk.left
                                anchors.leftMargin: buttonnew.width*0.4

                            }
                            Label{
                                id: numberNotOk
                                // width: parent.width*0.2; height: parent.height*0.2
                                text: booksunavailable; font { pixelSize: 15; bold: true }
                                anchors.top: bookNotOk.bottom
                                anchors.horizontalCenter: bookNotOk.horizontalCenter
                            }
                        }
                    }
                }
            }
        }
    }
}
