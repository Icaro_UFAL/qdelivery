import QtQuick 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
Item {
    id: windowControl
    height: root.height
    width: root.width

    SwipeView {
        id: swipeView
        anchors.fill: parent
        currentIndex: tabBar.currentIndex

        HomePage{

        }

        CollectionPage {

        }
        HistoricPage{

        }
        DonationPage{

        }
    }

    TabBar {
        id: tabBar
        currentIndex: swipeView.currentIndex
        anchors.bottom: windowControl.bottom
        width: parent.width
        background: Rectangle{
            color: "white"
        }

        TabButton {
            text: qsTr("Início")
        }
        TabButton {
            text: qsTr("Buscar")
        }
        TabButton {
            text: qsTr("Histórico")
        }
        TabButton{
            text: qsTr("Doação")
        }
    }





    /*StackView {
        id: stackAction
        initialItem: actionColumn
        anchors.fill: parent

    }

    Component{
        id: collectionPage
        CollectionPage{}

    }

   Image {
        id: userImage
        source: "qrc:/im/iconUser.png"
        anchors.left: windowHome.left
        anchors.top: windowHome.top
        height: 60
        width: 60
        anchors.leftMargin: 50
        anchors.topMargin: 30
    }
        Text {
            id: textsenha
            text: loginpage.sendName()
            color: "black"
            font.bold: true
            font.pointSize: 20
            anchors.left: windowHome.left
            anchors.top: userImage.bottom
            anchors.topMargin: 10
            anchors.leftMargin: 35
        }

    Column{
        id: columnHome
        anchors.top: userImage.bottom
        anchors.topMargin: windowHome.height*0.1
        anchors.left: windowHome.left
        width: (parent.width * 0.25)
        height: parent.height
        spacing: 2
        Button{
            id: principalButton
            anchors.left: columnHome.left
            font.bold: true
            text: "página inicial"
            contentItem: Text {
                      text: principalButton.text
                      font: principalButton.font
                      opacity: enabled ? 1.0 : 0.3
                      color: principalButton ? "#2f4f4f" : "#000000"
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      elide: Text.ElideRight
                  }
            background: Rectangle {
                     implicitWidth: columnHome.width
                     implicitHeight: columnHome.height*0.1
                     color: "#ffa500"
                     opacity: enabled ? 1 : 0.3
                     border.color: principalButton ? "#ffa500" : "#ffa500"
                     border.width: 1
                     radius: 5
                 }

        }
        Button{
            id: searchButton
            anchors.left: columnHome.left
            font.bold: true
            text: "Buscar Livro"
            contentItem: Text {
                      text: searchButton.text
                      font: searchButton.font
                      opacity: enabled ? 1.0 : 0.3
                      color: searchButton? "#2f4f4f" : "#000000"
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      elide: Text.ElideRight
                  }
            background: Rectangle {
                     implicitWidth: columnHome.width
                     implicitHeight: columnHome.height*0.1
                     color: "#ffa500"
                     opacity: enabled ? 1 : 0.3
                     border.color: searchButton? "#ffa500" : "#ffa500"
                     border.width: 1
                     radius: 5
                 }
            onClicked: {
                stackAction.push(collectionPage)
            }

        }
        Button{
            id: historicButton
            anchors.left: columnHome.left
            font.bold: true
            text: "Histórico"
            contentItem: Text {
                      text: historicButton.text
                      font: historicButton.font
                      opacity: enabled ? 1.0 : 0.3
                      color: historicButton? "#2f4f4f" : "#000000"
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      elide: Text.ElideRight
                  }
            background: Rectangle {
                     implicitWidth: columnHome.width
                     implicitHeight: columnHome.height*0.1
                     color: "#ffa500"
                     opacity: enabled ? 1 : 0.3
                     border.color: historicButton? "#ffa500" : "#ffa500"
                     border.width: 1
                     radius: 5
                 }

        }
        Button{
            id: donationButton
            anchors.left: columnHome.left
            font.bold: true
            text: "Doação"
            contentItem: Text {
                      text: donationButton.text
                      font: donationButton.font
                      opacity: enabled ? 1.0 : 0.3
                      color: donationButton? "#2f4f4f" : "#000000"
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      elide: Text.ElideRight
                  }
            background: Rectangle {
                     implicitWidth: columnHome.width
                     implicitHeight: columnHome.height*0.1
                     color: "#ffa500"
                     opacity: enabled ? 1 : 0.3
                     border.color: donationButton? "#ffa500" : "#ffa500"
                     border.width: 1
                     radius: 5
                 }

        }
    }
    Column{
        id: actionColumn
        anchors.left: columnHome.right
        anchors.top: windowHome.top
        anchors.bottom: windowHome.bottom
        anchors.right: windowHome.right
        Button{
            id: okButton
            font.bold: true
            anchors.centerIn: actionColumn
            text: "Histórico"
            contentItem: Text {
                      text: okButton.text
                      font: okButton.font
                      opacity: enabled ? 1.0 : 0.3
                      color: okButton? "#2f4f4f" : "#000000"
                      horizontalAlignment: Text.AlignHCenter
                      verticalAlignment: Text.AlignVCenter
                      elide: Text.ElideRight
                  }
            background: Rectangle {
                     implicitWidth: columnHome.width
                     implicitHeight: columnHome.height*0.1
                     color: "#ffa500"
                     opacity: enabled ? 1 : 0.3
                     border.color: okButton? "#ffa500" : "#ffa500"
                     border.width: 1
                     radius: 5
                 }

        }

    }*/

}
