#include "datacontroller.h"

Datacontroller::Datacontroller(QObject *parent) : QObject(parent)
{
    m_bookfilter = new QQmlObjectListModel<Book>(this);
}

bool Datacontroller::receiveLogin(QString email, QString password){

   Person *p;
   Database *base = Database::getInstance();
   Personcontroller *pcontroller = Personcontroller::getInstance();

   //procura usuario na base de dados
   for(int i =0; i<base->get_users()->size(); i++){

       p = base->get_users()->at(i);

       //se ja existir, retorna true
       if(p->get_email() == email && p->get_password() == password){
           pcontroller->set_actualUser(base->get_users()->at(i));
           emit exist();

           return true;
       }
   }

   emit notExist();
   return false;
}

bool Datacontroller::newRegister(QString firstName, QString lastName, QString email,
                            QString registration, QString password){

    Database* base = Database::getInstance();
    Person *p = new Person(firstName, lastName, email, registration, password);

    if(base->addUser(p)){
        emit success();
        return true;
    }

    else{
        emit notSuccess();
        return false;
    }
}

//adicionar livros no acervo
bool Datacontroller::newBookRegister(QString name, QString author, QString edition, int quantity){

    Database* base = Database::getInstance();
    QDate aux = QDate::currentDate();
    Book *b = new Book(name, author, edition, aux, quantity, this);

    if(base->addBook(b)){
        emit bookSuccess();
        return true;
    }

    else{
        emit bookNotSuccess();
        return false;
    }
}


