import QtQuick 2.0
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.1

Item{
    id: loginScreen
    height: root.height
    width: root.width

    Connections {
            target:datacontroller

            onExist: {                              //SLOTS SÓ PODEM COMEÇAR COM LETRA MAISCULAS
                console.log("Login feito.")
                stack.push(controlPage)

            }
            onNotExist:{
                texterror.visible = true
                console.log("Login incorreto.")
            }
    }

    /*LinearGradient {
                anchors.fill: parent
                start: Qt.point(0, 0)
                end: Qt.point(root.width, root.height)
                gradient: Gradient {
                    GradientStop { position: 0.2; color: "blue" }
                    GradientStop { position: 0.4; color: "white" }
                    GradientStop { position: 0.6; color: "white" }
                    GradientStop { position: 0.8; color: "red" }
                }
            }*/





    Rectangle {
        id: rectangleLoginScreen
        height: root.height
        width: root.width
        color: Material.primary

        Image{
            id: icLogos12
            anchors.right: rectangleLoginScreen.right
            anchors.rightMargin: 15
            anchors.top: rectangleLoginScreen.top
            anchors.topMargin: 15
            width: 125
            height: 125
            //fillMode: Image.PreserveAspectFit
            source: "qrc:/im/logo-ic.png"
        }

        Image{
            id: ufalLogo12
            anchors.left: rectangleLoginScreen.left
            anchors.leftMargin: 15
            anchors.top: rectangleLoginScreen.top
            anchors.topMargin: 15
            width: 85
            height: 125
            source: "qrc:/im/ufal-logo.png"
        }

        Pane {
            id: loginRectangle
            height: parent.height*0.4
            width: parent.height*0.4
            anchors.centerIn: parent
            Material.elevation: 15

            Column {
                id: loginColumn
                height: parent.height
                width: parent.height*0.8
                anchors.centerIn: parent
                spacing: 10

                Text {
                    id: textusuario
                    text: qsTr("Login")
                    color: "gray"
                    font.bold: true
                    font.pointSize: 18
                }

                TextField{
                    id: fieldusuario
                    placeholderText: qsTr("E-mail")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                }

                TextField{
                    id: fieldsenha
                    placeholderText: qsTr("Senha")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                    echoMode: TextInput.Password
                }
                Text{
                    id: texterror
                    text: qsTr("Acesso negado!")
                    color: Material.accent
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }

                Ibutton{

                    id: buttonentrar
                    sizewidth: parent.width
                    sizeheight: fieldsenha.height
                    text: "ENTRAR"
                    colorUp: Material.primary
                    colorDown: Material.accent
                    anchors.right: parent.right

                    action.onClicked: {// É UM SINAL E UM SLOT
                        var aux = datacontroller.receiveLogin(fieldusuario.text, fieldsenha.text)
                        fieldusuario.text = ''
                        fieldsenha.text = ''
                       /* if(aux){
                            console.log("tudo OK")
                            stack.push(homepage)
                        }
                        else{
                            texterror.visible = true
                            console.log("tudo errado")
                        }*/
                    }
                }

                Ibutton{

                    id: buttonnew
                    sizewidth: parent.width
                    sizeheight: fieldsenha.height
                    text: "CRIAR CONTA"
                    textColorUp: "gray"
                    textColorDown: "gray"
                    colorUp: "transparent"
                    colorDown: "#E0E0E0"
                    anchors.left: parent.left

                    action.onClicked: {// É UM SINAL E UM SLOT
                        stack.push(newUser)
                    }
                }

                /*Button{
                    id: buttonnew2
                    width: parent.width
                    text: "Criar Conta"
                    contentItem: Text {
                              text: buttonnew.text
                              font: buttonnew.font
                              opacity: enabled ? 1.0 : 0.3
                              color: buttonnew.down ? "#2f4f4f" : "#000000"
                              horizontalAlignment: Text.AlignHCenter
                              verticalAlignment: Text.AlignVCenter
                              elide: Text.ElideRight
                          }
                    background: Rectangle {
                            //implicitWidth: 100
                            implicitHeight: fieldsenha.height
                             //color: "#ffa500"
                             Material.accent: Material.Orange
                             opacity: enabled ? 1 : 0.3
                             border.color: buttonnew.down ? "#ffa500" : "#ffa500"
                             border.width: 1
                             radius: 5
                         }
                    onClicked: {
                        stack.push(newUser)
                    }
                }*/
            }
        }
    }
}
