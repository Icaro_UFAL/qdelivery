#ifndef PERSONCONTROLLER_H
#define PERSONCONTROLLER_H

#include <QObject>
#include <QDate>
#include "person.h"
#include "book.h"
#include "database.h"
class Personcontroller : public QObject
{

    Q_OBJECT

public:
     static Personcontroller* getInstance();
     Q_INVOKABLE QString sendFirstName();
     Q_INVOKABLE QString sendLastName();
     Q_INVOKABLE QString sendEmail();
     Q_INVOKABLE QString sendRegistration();
     Q_INVOKABLE Person* sendPerson();
     Q_INVOKABLE Book* sendBooks();
     Q_INVOKABLE double sendPenalty();
     Q_INVOKABLE bool rentBook(QString name, QString author, QString edition);
     Q_INVOKABLE bool historicUser(QString email);
     Q_INVOKABLE bool renewBook(QString name, QString author, QString edition);
     Q_INVOKABLE bool returnBook(QString name, QString author, QString edition);
     Q_INVOKABLE bool confirmKey(QString key);

protected:
     static Personcontroller* instance;
private:
    QML_WRITABLE_PROPERTY(Person*, actualUser)
    explicit Personcontroller(QObject *parent = nullptr);

signals:

public slots:
};

#endif // PERSONCONTROLLER_H
