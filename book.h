#ifndef BOOK_H
#define BOOK_H
#include "brunocppessencials/qqmlhelpers.h"
#include <QObject>
#include <QDate>
#include <QDate>
class Book : public QObject
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, name)
    QML_WRITABLE_PROPERTY(QString, author)
    QML_WRITABLE_PROPERTY(QString, edition)
    QML_WRITABLE_PROPERTY(bool, status)
    QML_WRITABLE_PROPERTY(int, maxRenew)
    QML_WRITABLE_PROPERTY(int, booksunavailable)
    QML_WRITABLE_PROPERTY(int, quantity)
    QML_WRITABLE_PROPERTY(double, penalty)
    QML_WRITABLE_PROPERTY(QString, dateRent)
    QML_WRITABLE_PROPERTY(QDate, qdateRent)
public:
    explicit Book(QString name, QString author, QString edition, QDate dateRent, int quantity, QObject *parent = nullptr);
signals:

public slots:
};

#endif // BOOK_H
