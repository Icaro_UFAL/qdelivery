import QtQuick 2.2

Item {
    property alias radius: button.radius
    property string colorUp: "#333333"
    property string colorDown: "#333333"
    property alias font: text.font
    property bool bold: false
    property alias pixelSize: text.font.pixelSize
    property alias text: text.text
    property string textColorUp: "#FFFFFF"
    property string textColorDown: "#FFFFFF"
   // property real pointSizeText: 10
    property alias action : mouseArea
    property real   borderWidth : 0
    property string borderColor : "#000000"
    property int sizewidth: button.width
    property int sizeheight: button.height
    property bool responsive : false
    height:sizeheight; width:sizewidth              // TENHO UM ITEM DO TAMANHO X invisivel, ai tudo que eu boto dentro tem que ter no máximo o tamanho dele

    Rectangle{
        id: button
        width: sizewidth
        height: sizeheight
        color: colorUp
        border.color: borderColor
        border.width: borderWidth
        radius: radius
        anchors.fill: parent
        Text {
            id: text
            text: text
            anchors.fill: parent
            color: textColorUp
            font.weight: Font.Medium
            font.pixelSize: button.height/2.5
            font.bold: bold
            horizontalAlignment: Text.AlignHCenter
            verticalAlignment: Text.AlignVCenter
        }
        MouseArea{
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: responsive
            onPressed: {
                text.color = textColorDown
                button.color = colorDown
            }
            onReleased: {
                text.color = textColorUp
                button.color = colorUp
            }

        }
    }

}
