import QtQuick 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.1

Item {
     id: windowRegister
     height: root.height
     width: root.width
     Connections {
             target:datacontroller
             onSuccess: {                              //SLOTS SÓ PODEM COMEÇAR COM LETRA MAISCULAS
                 console.log("Cadastro realizado.")

                 texterror.visible = false
                 textconfirm.visible = true

                 nameRegister.text = ""
                 lastNameRegister.text = ""
                 registryRegister.text = ""
                 emailRegister.text = ""
                 passwordRegister.text = ""
                 passwordConfirmationRegister.text = ""
             }
             onNotSuccess:{
                 console.log("Falha no cadastro.")

                 texterror.text = "Número de matrícula ou e-mail já cadastrado!"
                 texterror.visible = true
                 textconfirm.visible = false;
             }
     }

     Rectangle {

        height: root.height
        width: root.width
        color: Material.primary

        Pane {
            id: newRegisterRectangle
            height: parent.height*0.7
            width: parent.height*0.6
            anchors.centerIn: parent
            Material.elevation: 15

            Column {
                id: columnRegister
                anchors.centerIn: parent
                height: parent.height
                width: parent.height*0.7
                spacing: 10

                Text {
                    id: newRegisterText
                    text: qsTr("Cadastro de usuário")
                    color: "gray"
                    font.bold: true
                    font.pointSize: 18
                }

                TextField{
                    id: nameRegister
                    placeholderText: qsTr("Nome")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                }
                TextField{
                    id: lastNameRegister
                    placeholderText: qsTr("Sobrenome")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                }

                TextField{
                    id: registryRegister
                    placeholderText: qsTr("Número de matrícula")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                }
                TextField{
                    id: emailRegister
                    placeholderText: qsTr("Email institucional")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width

                }
                TextField{
                    id: passwordRegister
                    placeholderText: qsTr("Senha")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                    echoMode: TextInput.Password

                }
                TextField{
                    id: passwordConfirmationRegister
                    placeholderText: qsTr("Repita a senha")
                    focusReason: Qt.MouseFocusReason
                    width: parent.width
                    echoMode: TextInput.Password
                }
                Text{
                    id: textconfirm
                    text: qsTr("Cadastro realizado com sucesso")
                    color: "green"
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
                Text{
                    id: texterror
                    text: ""
                    color: "red"
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
                Ibutton{
                    id: registerButton
                    sizewidth: parent.width
                    sizeheight: passwordRegister.height
                    text: "CADASTRAR"
                    colorUp: Material.primary
                    colorDown: Material.accent

                    action.onClicked: {

                        //verifica se as senhas digitas nos dois campos são iguais

                        if(nameRegister.text == "") {
                            texterror.text = "Nome obrigatório!"
                            texterror.visible = true;
                        } else if(lastNameRegister.text == "") {
                            texterror.text = "Sobrenome obrigatório!"
                            texterror.visible = true;
                        } else if(registryRegister.text == "") {
                            texterror.text = "Matrícula obrigatória!"
                            texterror.visible = true;
                        } else if(emailRegister.text == "") {
                            texterror.text = "Email obrigatório!"
                            texterror.visible = true;
                        } else if(passwordRegister.text == "") {
                            texterror.text = "Senha obrigatória!"
                            texterror.visible = true;
                        } else if(passwordRegister.text != passwordConfirmationRegister.text) {
                            texterror.text = "As senhas não são iguais!"
                            texterror.visible = true;
                        } else {
                            var aux = datacontroller.newRegister(nameRegister.text,lastNameRegister.text, emailRegister.text, registryRegister.text,passwordRegister.text)
                        }

                   /* PODE FAZER ASSIM TBM    if(aux){
                            textconfirm.visible = true
                        }
                        else{

                        }*/

                    }
                }
                Ibutton {
                    id: backButton
                    sizewidth: parent.width
                    sizeheight: passwordRegister.height
                    text: "VOLTAR"
                    textColorUp: "gray"
                    textColorDown: "gray"
                    colorUp: "transparent"
                    colorDown: "#E0E0E0"

                    action.onClicked: {
                        stack.pop()
                    }
                }
            }
        }
    }
}
