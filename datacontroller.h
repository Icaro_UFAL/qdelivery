#ifndef DATACONTROLLER_H
#define DATACONTROLLER_H

#include <QObject>
#include <QDebug>
#include <iostream>
#include "database.h"
#include "person.h"
#include "personcontroller.h"
#include "book.h"
class Datacontroller : public QObject
{
    Q_OBJECT
    //QML_WRITABLE_PROPERTY(Person*, icaro)
    QML_OBJMODEL_PROPERTY(Book, bookfilter)
public:
    explicit Datacontroller(QObject *parent = nullptr);
    Q_INVOKABLE bool receiveLogin(QString email, QString password);
    Q_INVOKABLE bool newRegister(QString firstName,QString lastName,
                                 QString email,QString registration,QString password);
    Q_INVOKABLE bool newBookRegister(QString name, QString author, QString edition, int quantity);


signals:
    void exist();
    void notExist();
    void success();
    void notSuccess();
    void bookSuccess();
    void bookNotSuccess();
public slots:
};

#endif // DATACONTROLLER_H
