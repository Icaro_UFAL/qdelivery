#include <QApplication>               // PQ TIROU O GUI e tive que acrscentar a palavra widgtes na pasta .pro
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "datacontroller.h"
#include "personcontroller.h"
#include "person.h"


int main(int argc, char *argv[])
{
                                        // QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);       // PQ MUDAR DE UIAPPLICATION PARA QAPPLICATION
    QQmlApplicationEngine engine;
    Datacontroller *datacontroller = new Datacontroller();
    Personcontroller *personcontroller = Personcontroller::getInstance();
    Database *database = Database::getInstance();
    //Homepage *homepage = new Homepage();
    //Person *galeto = new Person();
    engine.rootContext()->setContextProperty("datacontroller", datacontroller);   //// DEIXA VISIVEL NO QML
    engine.rootContext()->setContextProperty("personcontroller", personcontroller);
    engine.rootContext()->setContextProperty("database", database);
    // engine.rootContext()->setContextProperty("homepage", homepage);
    engine.load(QUrl(QLatin1String("qrc:/main.qml")));             // JUNTA TUDO
    return app.exec();
}
    //QQmlContext *ctx = engine.rootContext();
    //ctx->setContextProperty("receiver", receiver);
