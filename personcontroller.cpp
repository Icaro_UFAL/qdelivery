#include "personcontroller.h"
#include <QDate>

Personcontroller::Personcontroller(QObject *parent) : QObject(parent)
{

}
Personcontroller* Personcontroller::instance = NULL;

Personcontroller* Personcontroller::getInstance(){
    if(instance == NULL){
        instance = new Personcontroller();
    }
    return instance;
}

QString Personcontroller::sendFirstName()
{
    return m_actualUser->get_firstName();

}

QString Personcontroller::sendLastName()
{
    return m_actualUser->get_lastName();
}

QString Personcontroller::sendEmail()
{
    return m_actualUser->get_email();
}

QString Personcontroller::sendRegistration()
{
    return m_actualUser->get_registration();
}

Person *Personcontroller::sendPerson()
{
    return m_actualUser;
}

Book* Personcontroller::sendBooks()
{
    for(int i = 0; i < m_actualUser->get_books()->size(); i++){
        Book *b = m_actualUser->get_books()->at(i);
        return b;
    }
}

double Personcontroller::sendPenalty()
{
    m_actualUser->updatePenalty();
    return m_actualUser->get_penalty();
}

bool Personcontroller::rentBook(QString name, QString author, QString edition){

      QDate aux = QDate::currentDate().addDays(7);
      Book *b = new Book(name, author, edition, aux, 1);
      Database* base = Database::getInstance();

      for(int i = 0; i < m_actualUser->get_books()->size(); i++) {
          Book *c = m_actualUser->get_books()->at(i);

          //se o usuario já alugou este livro, retorna falso
          if(b->get_name() == c->get_name() && b->get_author() == c->get_author() && b->get_edition() == c->get_edition()) {
              return false;
          }
      }

      base->linkedBook(m_actualUser, b, 1);
      return true;
}

bool Personcontroller::historicUser(QString email){

}

bool Personcontroller::renewBook(QString name, QString author, QString edition){

    QDate aux = QDate::currentDate();

    for(int i = 0; i < m_actualUser->get_books()->size(); i++) {
        Book *c = m_actualUser->get_books()->at(i);

        if(c->get_name() == name && c->get_author() == author && c->get_edition() == edition) {
            //verifica se não passou da data de devolução e possui menos de três renovações
            if(c->get_qdateRent() >= aux && c->get_maxRenew() < 3) {
                c->set_qdateRent(aux.addDays(5)); //incrementa dia atual em 7 e seta como nova data de devolução
                c->set_maxRenew(c->get_maxRenew()+1);
                c->set_dateRent(aux.addDays(5).toString("dd.MM.yyyy"));
                return true;
            }
        }
    }
    return false;
}
// ESSA FUNÇÃO ESTA CERTA? precisa verificar se tem multa.....
bool Personcontroller::returnBook(QString name, QString author, QString edition){

    QDate aux = QDate::currentDate();

    Book *b = new Book(name,author,edition, aux, 1); // QUANTIDADE QUANDO DEVOLVIDA, ANALISAR
    Database* base = Database::getInstance();
    base->linkedBook(m_actualUser,b, 2);
}

bool Personcontroller::confirmKey(QString key){
    if(key == "DIACOM"){
        return true;
    }
    return false;
}
