import QtQuick 2.0
import QtQuick 2.2
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

Item {
    id: windowHistoric
    height: root.height
    width: root.width
    Rectangle{
        id: historicRectangle
        width: parent.width
        height: parent.height
        color: "#B2DFDB"
        ListView {
            id:booksListView
            anchors.fill: parent
            model: personcontroller.actualUser.returnbooks
            spacing: historicRectangle.height*0.01
            cacheBuffer: 5
            snapMode: ListView.SnapToItem
            delegate: Rectangle {
                width:historicRectangle.width; height: historicRectangle.height*0.15
                color:"transparent"

                Rectangle {
                    width: parent.width*0.98
                    height: parent.height
                    color: "transparent"
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter

                    Image {
                        id: bookPhoto
                        width: parent.height*0.8
                        height: parent.height
                        source: "qrc:/im/iconNoPhoto.png"
                    }

                    Label {
                        id: titleLabel
                        width: parent.width*0.42; height: parent.height*0.4
                        text: name; font { pixelSize: 23 }
                        anchors.top: parent.top
                        anchors.topMargin: 5
                        anchors.left: bookPhoto.right
                        anchors.leftMargin: 10
                    }

                    Label {
                        id: authorLabel
                        width: parent.width*0.5; height: parent.height*0.2
                        text: author; font { pixelSize: 18 }
                        anchors.left: bookPhoto.right
                        anchors.leftMargin: 10
                        anchors.verticalCenter: bookPhoto.verticalCenter
                    }

                    Label {
                        id: editionLabel
                        width: parent.width*0.5; height: parent.height*0.2
                        text: edition; font { pixelSize: 18 }
                        anchors.left: bookPhoto.right
                        anchors.leftMargin: 10
                        anchors.bottom: parent.bottom
                        anchors.bottomMargin: 3
                    }
                }
            }
        }
    }
}
