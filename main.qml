import QtQuick 2.8
import QtQuick.Controls 2.1
//import QtQuick.Controls.Material 2.1
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0

ApplicationWindow {
    id: root
    visible: true
    width: 1024
    height: 768
    title: qsTr("Sistema de Biblioteca")

       StackView {
           id: stack
           initialItem: mainView
           anchors.fill: parent
       }

       Component{
           //AO chamar a classe Login, login fica dentro de ApplicationWindow? ou pega os atributos de tamanho? eu preciso ancorar no pai? o pai de collun é o item ou ApplicationWindow
           id: mainView
           Login{}
       }

       Component{
           id: newUser
           NewRegister{}

       }

       Component{
           id: controlPage
           ControlPage{}
       }

  /*  Image {
        id: backimage
        source:"im/back4.jpg"
        anchors.fill: parent
        fillMode: Image.PreserveAspectCrop
    }*/
}
