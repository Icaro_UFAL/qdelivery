import QtQuick 2.0
import QtQuick 2.2
import QtQuick 2.8
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import QtQuick.Controls.Material 2.1

Item {
    id: windowHome
    height: root.height
    width: root.width

    Rectangle{
        width: 300
        height: 300
        anchors.centerIn: windowHome
        Popup{
            id: homePopup

            property var book: Object

            function validate(nameBook, authorBook, editionBook) {
                book["nameBook"] = nameBook
                book["authorBook"] = authorBook
                book["editionBook"] = editionBook
                homePopup.open()
            }

            width: parent.width
            height: parent.height
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Column{
                anchors.centerIn: parent
                spacing: 5
                TextField{
                    id: adminKey
                    placeholderText: "Chave do DIACOM"
                    echoMode: TextInput.Password
                    width: homePopup.width*0.8

                }
                TextField{
                    id: adminName
                    placeholderText: "Responsável do DIACOM"
                    width: homePopup.width*0.8

                }
                Button{
                    id: adminButton
                    text: "Validar"
                    width: homePopup.width*0.8
                    onClicked: {
                        if(personcontroller.confirmKey(adminKey.text)){
                            personcontroller.returnBook(homePopup.book["nameBook"],homePopup.book["authorBook"],homePopup.book["editionBook"])
                            homePopup.close()
                            texterror.visible = false;
                            adminKey.text = "";
                            adminName.text = "";

                        }
                        else{
                            texterror.visible = true;
                        }
                    }
                }
                Text{
                    id: texterror
                    text: qsTr("Chave Errada!")
                    color: Material.accent
                    font.bold: true
                    font.pointSize: 10
                    visible: false
                }
            }
        }
    }

    Rectangle{
        width: 300
        height: 300
        anchors.centerIn: windowHome
        Popup{
            id: errorRenew
            width: parent.width
            height: parent.height
            modal: true
            focus: true
            closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
            Label {
                anchors.centerIn: parent
                id: errorTextRenew
                font.bold: true
                color: "Red"
                text: qsTr("NÃO É POSSÍVEL RENOVAR." + "\n" + "LIVRO COM MULTA OU \nEXCEDEU O LIMITE DE RENOVAÇÃO")
            }
        }
    }




    Rectangle {
        height: root.height
        width: root.width
        color: "#B2DFDB"

        Column{
            id: columnHome
            width: parent.width
            height: parent.height
            Item{
                id: columnPerfilHome
                width: parent.width
                height: parent.height*0.3

                Label {
                    id: userLabel
                    width: parent.width*0.5; height: parent.height*0.1
                    text: "Usuário(a): " + personcontroller.sendFirstName(); font { pixelSize: 23 }
                    anchors.top: parent.top
                    anchors.topMargin: 15
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                }
                Label {
                    id: registrationLabel
                    width: parent.width*0.5; height: parent.height*0.1
                    text: "Matrícula: " + personcontroller.sendRegistration(); font { pixelSize: 23 }
                    anchors.top: userLabel.bottom
                    anchors.topMargin: 15
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                }
                Label {
                    id: penaltyLabel
                    width: parent.width*0.5; height: parent.height*0.1
                    text: "Multa: R$ " + personcontroller.sendPenalty(); font { pixelSize: 23 }
                    anchors.top: registrationLabel.bottom
                    anchors.topMargin: 15
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                }
                Button {
                    id: logoutButton
                    width: 50
                    height: 50
                    anchors.top: parent.top
                    anchors.topMargin: 20
                    anchors.right: parent.right
                    anchors.rightMargin: 25
                    background: Rectangle {
                        color: "transparent"
                        Image {
                            id: logoutImage
                            anchors.top: parent.top
                            source: "qrc:/im/iconLogout.png"
                            width: parent.width
                            height: parent.width
                            fillMode: Image.PreserveAspectFit

                        }
                        Text {
                            id: returnText
                            text: "SAIR"
                            color: "black"
                            font.pixelSize: 13
                            anchors.top: logoutImage.bottom
                            anchors.topMargin: 5
                            anchors.horizontalCenter: parent.horizontalCenter
                        }
                     }
                    onClicked: {
                        stack.pop()
                    }
                }

                Label {
                    id: rentLabel
                    width: parent.width*0.5; height: parent.height*0.1
                    text: "LIVROS RESERVADOS:"; font { pixelSize: 26 }
                    anchors.bottom: parent.bottom
                    anchors.bottomMargin: 20
                    anchors.left: parent.left
                    anchors.leftMargin: 15
                }
            }
            Rectangle {
                id: listItem
                width: parent.width; height:parent.height*0.7
                clip: true
                color: "transparent"
                ListView {
                    id:booksListView
                    anchors.fill: parent
                    model: personcontroller.actualUser.books
                    spacing: columnHome.height*0.01
                    cacheBuffer: 5
                    snapMode: ListView.SnapToItem
                    delegate: Rectangle {
                        width:columnHome.width; height: columnHome.height*0.15
                        color:"transparent"

                        Rectangle {
                            width: parent.width*0.98
                            height: parent.height
                            color: "transparent"
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter

                            Image {
                                id: bookPhoto
                                width: parent.height*0.8
                                height: parent.height
                                source: "qrc:/im/iconNoPhoto.png"
                            }

                            Label {
                                id: titleLabel
                                width: parent.width*0.42; height: parent.height*0.4
                                text: name; font { pixelSize: 23 }
                                anchors.top: parent.top
                                anchors.topMargin: 5
                                anchors.left: bookPhoto.right
                                anchors.leftMargin: 10
                            }

                            Label {
                                id: authorLabel
                                width: parent.width*0.5; height: parent.height*0.2
                                text: author; font { pixelSize: 18 }
                                anchors.left: bookPhoto.right
                                anchors.leftMargin: 10
                                anchors.verticalCenter: bookPhoto.verticalCenter
                            }

                            Label {
                                id: editionLabel
                                width: parent.width*0.5; height: parent.height*0.2
                                text: edition; font { pixelSize: 18 }
                                anchors.left: bookPhoto.right
                                anchors.leftMargin: 10
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 3
                            }

                            Label {
                                id: returnDateLabel
                                width: 200; height: parent.height*0.2
                                text: "Devolver até: " + dateRent; font { pixelSize: 18 }
                                anchors.right: parent.right
                                anchors.top: parent.top
                                anchors.topMargin: 10
                            }

                            Button {
                                id: returnBookButton
                                width: 40
                                height: parent.height*0.6
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 5
                                anchors.right: parent.right
                                anchors.rightMargin: 25
                                background: Rectangle {
                                    color: "transparent"
                                    Image {
                                        id: returnImage
                                        anchors.top: parent.top
                                        source: "qrc:/im/iconReturn.png"
                                        width: parent.width
                                        height: parent.width
                                        fillMode: Image.PreserveAspectFit

                                    }
                                    Text {
                                        id: returnnText
                                        text: "DEVOLVER"
                                        color: "red"
                                        font.pixelSize: 13
                                        anchors.top: returnImage.bottom
                                        anchors.topMargin: 5
                                        anchors.horizontalCenter: parent.horizontalCenter
                                    }
                                 }
                                onClicked: {
                                    homePopup.validate(titleLabel.text, authorLabel.text, editionLabel.text)
                                }
                            }

                            Button {
                                id: renewBookButton
                                width: 40
                                height: parent.height*0.6
                                anchors.bottom: parent.bottom
                                anchors.bottomMargin: 5
                                anchors.right: returnBookButton.left
                                anchors.rightMargin: 85
                                background: Rectangle {
                                    color: "transparent"
                                    Image {
                                        id: renewImage
                                        anchors.top: parent.top
                                        source: "qrc:/im/iconRenew.png"
                                        width: parent.width
                                        height: parent.width
                                        fillMode: Image.PreserveAspectFit

                                    }
                                    Text {
                                        id: renewText
                                        text: "RENOVAR"
                                        color: "green"
                                        font.pixelSize: 13
                                        anchors.top: renewImage.bottom
                                        anchors.topMargin: 5
                                        anchors.horizontalCenter: parent.horizontalCenter
                                    }
                                 }
                                onClicked: {
                                    //chamar metodo de renovar para o livro associado
                                    if(!personcontroller.renewBook(titleLabel.text, authorLabel.text, editionLabel.text)){
                                                errorRenew.open()
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}
