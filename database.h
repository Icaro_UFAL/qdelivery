#ifndef DATABASE_H
#define DATABASE_H
#include "person.h"
#include "book.h"
#include "brunocppessencials/qqmlhelpers.h"
#include "brunocppessencials/qqmlobjectlistmodel.h"
#include <QObject>
// Classe Login é criado com padrão singleton
class Database : public QObject
{
    Q_OBJECT
public:
    static Database* getInstance();                       // Responsável por pegar a instância do objeto Login
    bool addUser(Person *p);                             // Método de adicionar um usuário no banco
    bool linkedBook(Person *p, Book *b, int type);       // Método para linkar o livro com a pessoa. 1:Alugar, 2:Desalugar
    bool addBook(Book *b);                               // Método de adicionar livro no banco
    //
protected:
    static Database* instance;                     //Instância privada Login

private:
    QML_OBJMODEL_PROPERTY(Person, users)                 // Lista de todos os usuários
    QML_OBJMODEL_PROPERTY(Book, allbooks)                // Lista de todos os livros do acervo
    Database(QObject *parent = nullptr);

signals:

public slots:

};

#endif // DATABASE_H
