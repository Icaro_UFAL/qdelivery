#ifndef PERSON_H
#define PERSON_H
#include "brunocppessencials/qqmlobjectlistmodel.h"
#include "brunocppessencials/qqmlhelpers.h"
#include "book.h"
#include <QObject>
#include <QDate>

class Person : public QObject
{
    Q_OBJECT
    QML_WRITABLE_PROPERTY(QString, firstName)
    QML_WRITABLE_PROPERTY(QString, lastName)
    QML_WRITABLE_PROPERTY(QString, email)
    QML_WRITABLE_PROPERTY(QString, registration)
    QML_WRITABLE_PROPERTY(QString, password)
    QML_WRITABLE_PROPERTY(double, penalty)
    QML_OBJMODEL_PROPERTY(Book, books)
    QML_OBJMODEL_PROPERTY(Book, returnbooks)
public:
    explicit Person(QString firstName, QString lastName,QString email,
                    QString registration,QString password, QObject *parent = nullptr);
    void NewBook(QString name, QString author, QString edition, QDate dateRent, int quantity);
    void RenewBook(QString name, QString author, QString edition, QDate dateRent, int quantity);
    void updatePenalty();                               //como fazer isso???
    void Historic();
    void returnedBook(QString name, QString author, QString edition);
signals:

public slots:
};

#endif // PERSON_H
